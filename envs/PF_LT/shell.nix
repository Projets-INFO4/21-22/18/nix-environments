{ pkgs ? import <nixpkgs> {} }:

pkgs.stdenv.mkDerivation {
  name = "pf_lt-env";
  buildInputs = with pkgs; [
  ocaml
  emacsPackagesNg.tuareg
  coq


  ];
  hardeningDisable = [ "all" ];
}
