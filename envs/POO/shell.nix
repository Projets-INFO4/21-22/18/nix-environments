{ pkgs ? import <nixpkgs> {} }:

pkgs.stdenv.mkDerivation {
  name = "poo-env";
  buildInputs = with pkgs; [
    adoptopenjdk-bin
    eclipses.eclipse-java


  ];
  hardeningDisable = [ "all" ];
}
