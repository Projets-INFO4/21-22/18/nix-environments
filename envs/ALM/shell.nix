{ pkgs ? import <nixpkgs> {} }:

let
  lustre = pkgs.stdenv.mkDerivation rec {
      name = "lustreV4";

      src = builtins.fetchurl {
        
        url = https://www-verimag.imag.fr/DIST-TOOLS/SYNCHRONE/lustre-v4/distrib/linux64/lustre-v4-III-e-linux64.tgz;
        sha256 = "0f137fynmn01gaw64pbg3wwgh294qrxvrqj452kvshifg0hzg7d8";
      };

      installPhase = ''
        mkdir -p $out/bin
        cd bin
        cp * $out/bin/
        chmod +x $out/bin/*
      '';

    };
in
pkgs.stdenv.mkDerivation rec {
  name = "ALM1";
  shellHook = ''
      echo -e "\e[32m=============================================\e[0m"
      echo -e "\e[32m=== Welcome to ALM1 environment ===\e[0m"
      echo -e "\e[32m=============================================\e[0m"
    '';
  hardeningDisable = [ "all" ];
  buildInputs = [ lustre pkgs.qemu];
}
