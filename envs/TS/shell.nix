{ pkgs ? import <nixpkgs> {} }:

pkgs.stdenv.mkDerivation {
  name = "ts-env";
  buildInputs = with pkgs; [
    python
    vscode


  ];
  hardeningDisable = [ "all" ];
}
