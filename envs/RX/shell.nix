{ pkgs ? import <nixpkgs> {} }:

pkgs.stdenv.mkDerivation {
  name = "rx-env";
  buildInputs = with pkgs; [
    minicom
    gns3-gui
    wireshark

  ];
  hardeningDisable = [ "all" ];
}
