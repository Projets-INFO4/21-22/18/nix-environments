{ pkgs ? import <nixpkgs> {} }:

pkgs.stdenv.mkDerivation {
  name = "pw-env";
   shellHook = ''
      echo -e "Welcome to the PW environment \n
      This nix shell offer all you need to work on web programmation
      If httpd server doesn't work,
      first use:
      find /nix/store/ -type f -name "httpd.conf" 
      to locate the /conf/httpd.conf file
      add to the file this:
      ServerName 127.0.0.1
      Next, find the binary of http:
      find /nix/store/ -type f -wholename "*/bin/httpd"
      And use the command: 
      sudo /nix/store/previouslyFindFolder/bin/httpd -k start
      Start could be replaced by stop to stop the server
      You can now try to connect to http://localhost"
    '';

  buildInputs = with pkgs; [
    apacheHttpd
    php74
    php74Extensions.curl
    php74Extensions.mbstring

  ];
  hardeningDisable = [ "all" ];
}
