{ pkgs ? import <nixpkgs> {} }:

pkgs.stdenv.mkDerivation {
  name = "ps-env";
  buildInputs = with pkgs; [
    rWrapper
    rstudioWrapper


  ];
  hardeningDisable = [ "all" ];
}
