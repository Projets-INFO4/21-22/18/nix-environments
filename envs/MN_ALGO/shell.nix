{ pkgs ? import <nixpkgs> {} }:

pkgs.stdenv.mkDerivation {
  name = "mn_algo-env";
  buildInputs = with pkgs; [
    hwloc
    htop


  ];
  hardeningDisable = [ "all" ];
}
